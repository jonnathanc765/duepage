
const mix = require('laravel-mix');

// mix.autoload({
//     'jquery': ['jQuery', '$', 'jquery'],
// })

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix
   .js('resources/js/app.js', 'public/js')
   .js('resources/js/script.js', 'public/js')

   .js('resources/js/calculator/bundle.js', 'public/js/calculator.js')

   .js('resources/js/sweetalert.js', 'public/js')

   .sass('resources/sass/app.scss', 'public/css')

   .sass('resources/sass/styles.scss', 'public/css')

   .sass('resources/sass/page.scss', 'public/css')

   .sass('resources/sass/font-awesome.scss', 'public/css');

