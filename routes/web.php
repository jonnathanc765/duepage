<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/', 'Frontend\HomeController@index')->name('home');
Route::get('/duepage', 'Frontend\HomeController@page')->name('duepage');
Route::get('/home', function ()
{
    return redirect()->route('home');
});



Route::prefix('administrator')->group(function ()
{
    Route::group(['middleware' => ['auth']], function () {

        Route::get('/', 'Backend\HomeController@index')->name('admin.index');

        Route::prefix('resources')->group(function ()
        {
            Route::get('/', 'Backend\ResourcesController@index')->name('admin.resources.index');
        });

        Route::prefix('contact')->group(function ()
        {
            Route::get('/', 'Backend\ContactsController@index')->name('admin.contacts.index');
        });

    });
});

Route::post('resources/store', 'Backend\ResourcesController@store')->name('resources.store');
Route::post('contacts/store', 'Backend\ContactsController@store')->name('contacts.store');
