<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\CreateResourceRequest;
use App\Mail\ResourceMail;
use App\Mail\ResourceOneMail;
use App\Mail\ResourceThreeMail;
use App\Mail\ResourceTwoMail;
use App\Resource;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class ResourcesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $resources = Resource::orderBy('read','ASC')->get();

        $reads = Resource::where('read', false)->get();

        foreach ($reads as $read) {
            $read->update(['read' => true]);
        }

        return view('admin.resources.index', compact('resources'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
     public function store(CreateResourceRequest $request)
    {
        $data = $request->validated();
        $obj = new \stdClass;


        $obj->name = $data['name'];
        $obj->email = $data['email'];
        $obj->phone = $data['phone'];
        $obj->resource = $data['resource'];


        if ($data['resource'] == 0) {
            $data['resource'] = 'Limites de bancos venezolanos';
            $obj->resource = 'Limites de bancos venezolanos';
            try {
                Mail::to($data['email'])->send(new ResourceOneMail($obj));
            } catch (\Throwable $th) {
                return 'error';
            }
        } elseif ($data['resource'] == 1) {
            $data['resource'] = 'Limites de transferencias platafoma Zelle';
            $obj->resource = 'Limites de transferencias platafoma Zelle';
            try {
                Mail::to($data['email'])->send(new ResourceTwoMail($obj));
            } catch (\Throwable $th) {
                return 'error';
            }
        } elseif ($data['resource'] == 2) {
            $data['resource'] = 'Comisiones de transferencias Paypal';
            $obj->resource = 'Comisiones de transferencias Paypal';
            try {
                Mail::to($data['email'])->send(new ResourceThreeMail($obj));
            } catch (\Throwable $th) {
                return 'error';
            }
        } else {
            return back()->withError('Ha ocurrido un error, intenta de nuevo');
        }

        Resource::create([
            "name" => $data['name'],
            "email" => $data['email'],
            "phone" => $data['phone'],
            "resource" => $data['resource']
        ]);

        try {
            Mail::to(env('MAIL_TO'))->send(new ResourceMail($obj));
        } catch (\Throwable $th) {
            return 'error';
        }

        return redirect()->route('duepage')->withSuccess('Tu recurso se ha enviado a tu correo, revisa tu bandeja de entrada o spam');;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Resource  $resource
     * @return \Illuminate\Http\Response
     */
    public function show(Resource $resource)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Resource  $resource
     * @return \Illuminate\Http\Response
     */
    public function edit(Resource $resource)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Resource  $resource
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Resource $resource)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Resource  $resource
     * @return \Illuminate\Http\Response
     */
    public function destroy(Resource $resource)
    {
        //
    }
    public function getDownload($file_name)
    {
        $file = public_path(). '/downloads/' . $file_name.".pdf";

        $headers = array(
                'Content-Type: application/pdf',
                );

        return response()->download(public_path() . "/downloads/" . $file_name, $headers);
    }
}
