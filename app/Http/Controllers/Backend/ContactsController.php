<?php

namespace App\Http\Controllers\Backend;

use App\Contact;
use App\Http\Controllers\Controller;
use App\Http\Requests\CreateContactRequest;
use App\Mail\ContactMail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class ContactsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $contacts = Contact::orderBy('read', 'ASC')->get();

        $reads = Contact::where('read', false)->get();

        foreach ($reads as $read) {
            $read->update(['read' => true]);
        }

        return view('admin.contacts.index', compact('contacts'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateContactRequest $request)
    {
        $data = $request->validated();

        $obj = new \stdClass;

        $obj->name = $data['name'];
        $obj->email = $data['email'];
        $obj->phone = $data['phone'];
        $obj->subject = $data['subject'];
        $obj->message = $data['message'];

        try {
            Mail::to(env('MAIL_TO'))->send(new ContactMail($obj));
        } catch (\Throwable $th) {
            return 'Error';
        }

        Contact::create($data);

        return redirect()->route('home')->withSuccess('Su mensaje se ha enviado exitosamente, nos pondremos en contacto contigo los mas pronto posible');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Contact  $contact
     * @return \Illuminate\Http\Response
     */
    public function show(Contact $contact)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Contact  $contact
     * @return \Illuminate\Http\Response
     */
    public function edit(Contact $contact)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Contact  $contact
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Contact $contact)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Contact  $contact
     * @return \Illuminate\Http\Response
     */
    public function destroy(Contact $contact)
    {
        //
    }
}
