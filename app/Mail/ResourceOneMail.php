<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ResourceOneMail extends Mailable
{
    use Queueable, SerializesModels;

    public $resource;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($resource)
    {
        $this->resource = $resource;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

        return $this->from('info@cesarsanchez.com')
                    ->view('mails.resource-send')
                    ->subject('Limites de bancos venezolanos')
                    ->attach(public_path() . '/' . 'downloads/7652739e-1200-4b83-9f12-eef97c0d11c2.JPG');
    }
}
