<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ResourceTwoMail extends Mailable
{
    use Queueable, SerializesModels;

    public $resource;


    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($resource)
    {
        $this->resource = $resource;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

        return $this->from('info@cesarsanchez.com')
                    ->view('mails.resource-send')
                    ->subject('Limites de transferencias platafoma Zelle')
                    ->attach(public_path() . '/' . 'downloads/74e48902-0700-4939-ae92-9c76f93c1ac6.JPG');
    }
}
