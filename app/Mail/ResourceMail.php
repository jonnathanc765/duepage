<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ResourceMail extends Mailable
{
    use Queueable, SerializesModels;
    public $resource;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($resource)
    {
        $this->resource = $resource;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('info@cesarsanchez.com')
                    ->subject('Han descargado un recurso desde tu web')
                    ->view('mails.resource');
    }
}
