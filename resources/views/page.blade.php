@extends('layouts.app')

@section('head')
<!-- Customimized Style -->
    <link rel="stylesheet" href="{{ asset('css/styles.css') }}">
    <link rel="stylesheet" href="{{ asset('css/page.css') }}">
@endsection

@section('content')

    <header id="app">


        <div class="rotate-element">
            <img src="{{ asset('img/resources/Recurso 31.png') }}" alt="">
        </div>

        <div class="container">
            <nav class="navbar navbar-expand-lg navbar-dark">
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav mr-auto social">
                        <li>
                            <a href="http://www.instagram.com/cesanchezu" target="_blank"><i class="fab fa-instagram"></i></a>
                        </li>
                        <li>
                            <a href="https://wa.me/584245670668" target="_blank"><i class="fab fa-whatsapp"></i></a>
                        </li>
                        <li>
                            <a href="https://www.linkedin.com/in/cesanchezu/" target="_blank"><i class="fab fa-linkedin"></i></a>
                        </li>
                    </ul>
                    <ul class="navbar-nav menu">
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('home') }}">Inicio <span class="sr-only">(current)</span></a>
                        </li>
                        <li class="nav-item active">
                            <a class="nav-link" href="#calculator">Calculadora Paypal <span class="sr-only">(current)</span></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link smooth" data-scroll href="#resources">Descargas <span class="sr-only"></span></a>
                        </li>
                    </ul>
                </div>
            </nav>
        </div>


        <div class="container">
            <calculator-form></calculator-form>
        </div>


    </header>

    <main>
        <section class="resources">
            <div class="container" id="resources">
                <div class="row">
                    <div class="col-md-12">
                        <h2 class="text-center">Descarga recursos bancarios</h2>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="card shadow">
                            <img src="{{ asset('img/Recurso-28.png') }}" class="card-img-top" alt="...">
                            <div class="card-body">
                                <h5 class="card-title">Limites de bancos venezolanos</h5>
                                {{-- <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p> --}}
                            <div class="d-flex justify-content-center">
                                <a href="#resources-form" onclick="update(0, 'Limites de bancos venezolanos')" class="smooth">Descargar</a>
                            </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                    <div class="card shadow">
                        <img src="{{ asset('img/Recurso-29.png') }}" class="card-img-top" alt="...">
                            <div class="card-body">
                                <h5 class="card-title">Limites de transferencias platafoma Zelle</h5>
                                {{-- <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p> --}}
                                <div class="d-flex justify-content-center">
                                    <a href="#resources-form" onclick="update(1, 'Limites de transferencias platafoma Zelle')" class="smooth">Descargar</a>
                                </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card shadow">
                        <img src="{{ asset('img/Recurso-30.jpg') }}" class="card-img-top" alt="...">
                            <div class="card-body">
                                <h5 class="card-title">Comisiones de transferencias Paypal</h5>
                                {{-- <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p> --}}
                                <div class="d-flex justify-content-center">
                                    <a href="#resources-form" onclick="update(2, 'Comisiones de transferencias Paypal')" class="smooth">Descargar</a>
                                </div>
                        </div>
                    </div>
                </div>
                </div>
            </div>

            <svg id="gray-waves" data-name="Capa 7" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1366 139.99"><defs><style>.cls-1{fill:#f0f;}</style></defs><title>curvas cesar</title><path class="cls-1" d="M1365,632c-336.42,154.76-457.28,38.13-881,3C65,600.26,0,706.35,0,706.35V768H1366Z" transform="translate(0 -628.01)"/></svg>

        </section>

        <section class="resources-form" id="resources-form">

            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                         <h2 class="text-center resource-text">
                             Límites de bancos venezolanos
                         </h2>
                         <p class="text-center">
                             Puedes elegir otro recurso en la seccion anterior
                         </p>
                    </div>
                </div>
                <div class="row d-flex justify-content-center">
                    <div class="col-md-7">
                        <form action="{{ route('resources.store') }}" method="POST">
                            @csrf
                            <input type="hidden" value="{{ old('resource', 0) }}" name="resource" id="resource" value="0">
                            <div class="form-group">
                                <input value="{{ old('name') }}" type="text" name="name" class="form-control @error('name') is-invalid @enderror" placeholder="Nombre">
                                @error('name')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div>
                                @enderror
                            </div>
                            <div class="form-group">
                                <input value="{{ old('email') }}" type="email" class="form-control @error('email') is-invalid @enderror" name="email" placeholder="Email">
                                @error('email')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div>
                                @enderror
                            </div>
                            <div class="form-group">
                                <input value="{{ old('phone') }}" type="text" class="form-control @error('phone') is-invalid @enderror" name="phone" placeholder="Teléfono">
                                @error('phone')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div>
                                @enderror
                            </div>

                            <div class="form-group checkbox-group">
                                <input disabled checked type="checkbox" id="term">
                                <label for="term">Al marcar esto, acepta nuestros términos y política de privacidad </label>
                            </div>

                            <div class="d-flex justify-content-end">
                                <button type="button" class="btn btn-submit submit">Enviar mensaje</button>
                            </div>


                        </form>
                    </div>
                </div>
            </div>


        </section>

    </main>




    @include('layouts.footer')
@endsection

@section('scripts')
    <script src="{{ asset('js/calculator.js') }}"></script>
    <script>

        function update(resource, description) {

            $('.resource-text').text(description);
            $('#resource').val(resource);

        }

    </script>
@endsection
