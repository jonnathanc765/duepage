<footer>

    <div class="container">
        <div class="row">
            <div class="col-md-4 col-sm-12 col-12 description mb-4">
                <h3>César Sánchez</h3>
                <p class="mt-3">
                    Economista, especialista en finanzas y banca. El propósito de mi trabajo es crear estrategias financieras para mis clientes con el objetivo maximizar sus finanzas y optimizar su modelo de negocio.
                </p>
            </div>
            <div class="col-md-4 col-sm-6 links">
                <h4>Enlaces ràpidos</h4>

                <ul>
                    <li><a href="#">Conóceme</a></li>
                    <li><a href="#">Testimoniales</a></li>
                    <li><a href="#">Contáctame</a></li>
                    <li><a href="#">Preguntas Frecuentes</a></li>
                </ul>
            </div>
            <div class="contact-info col-md-4 col-sm-6 col-12">
                <h4>Contacto</h4>
                <ul>
                    <li>
                        <span><i class="fas fa-envelope"></i></span>
                        <div>
                            <a href="mailto:cesarsan92@gmail.com">Correo</a>
                            <a href="mailto:cesarsan92@gmail.com">cesarsan92@gmail.com</a>
                        </div>
                    </li>
                    <li>
                        <span><i class="fas fa-phone-square-alt"></i></span>
                        <div>
                            <a href="tel:+19783949881">Teléfono</a>
                            <a href="tel:+19783949881">+1 (978) 394-9881</a>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>

</footer>
