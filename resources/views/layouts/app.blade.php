<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">

    <meta name="theme-color" content="#1a1a1a">

    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name') }}</title>

    <!-- Favicon -->
    <link rel="shortcut icon" href="{{ asset('img/favicon.png') }}">

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet">

    <!-- Fonts -->
    <link href="{{ asset('css/font-awesome.css') }}" rel="stylesheet"></head>

    <!-- Owl Carousel -->
    <link rel="stylesheet" href="{{ asset('vendor/OwlCarousel/assets/owl.carousel.min.css') }}">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    @yield('head')

</head>
<body>

    @yield('content')

    <!-- Scripts -->
    {{-- Jquery, Popper and Bootstrap --}}
    <script src="{{ asset('js/app.js') }}" defer></script>

    <script src="{{ asset('vendor/SmothScroll/smothscroll.min.js') }}"></script>

    <script>
        var scroll = new SmoothScroll('a[href*="#"].smooth', {
            speed: 500
        });



    </script>

    @include('components.sweet')

    @yield('scripts')

</body>
</html>
