<div class="container">

    <nav class="navbar navbar-expand-md navbar-dark">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto social">
                <li>
                    <a href="http://www.instagram.com/cesanchezu" target="_blank"><i class="fab fa-instagram"></i></a>
                </li>
                <li>
                    <a href="https://wa.me/584245670668" target="_blank"><i class="fab fa-whatsapp"></i></a>
                </li>
                <li>
                    <a href="https://www.linkedin.com/in/cesanchezu/" target="_blank"><i class="fab fa-linkedin"></i></a>
                </li>
            </ul>
            <ul class="navbar-nav">
                <li class="nav-item active">
                    <a class="nav-link smooth" href="#meet-me" data-scroll>Conóceme <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link smooth" href="#testimonials" data-scroll>Testimoniales <span class="sr-only"></span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link smooth" href="#contact-me" data-scroll>Contáctame <span class="sr-only"></span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link smooth" href="#faqs" data-scroll>Preguntas Frecuentes <span class="sr-only"></span></a>
                </li>
            </ul>
        </div>
    </nav>
</div>
