<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <a class="navbar-brand" href="#">
        <img src="{{ asset('img/favicon.png') }}" alt="alt" width="30px">
        {{ config('app.name') }}
    </a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarHeader" aria-controls="navbarHeader" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    @auth

    <div class="collapse navbar-collapse" id="navbarHeader">

        <ul class="navbar-nav mr-auto">
            <li class="nav-item @yield('nav.admin')">
                <a class="nav-link" href="{{ route('admin.index') }}">Inicio <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item @yield('nav.admin.resources')">
                <a class="nav-link" href="{{ route('admin.resources.index') }}">Recursos</a>
            </li>
            <li class="nav-item @yield('nav.admin.contacts')">
                <a class="nav-link" href="{{ route('admin.contacts.index') }}">Contactos</a>
            </li>
        </ul>

        <div class="dropdown mr-5">
            <a class="btn btn-info dropdown-toggle" href="#" role="button" id="dropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                {{ Auth::user()->name ? Auth::user()->name : 'Usuario' }}
            </a>

            <div class="dropdown-menu" aria-labelledby="dropdown">
                {{-- <a class="dropdown-item" href="#">Action</a> --}}
                {{-- <a class="dropdown-item" href="#">Another action</a> --}}
                <a class="dropdown-item" href="#" onclick="document.getElementById('logout-form').submit()">Cerrar sesión</a>
            </div>
            <form action="{{ route('logout') }}" id="logout-form" method="POST" style="display:none;">
                @csrf
            </form>
        </div>

    </div>
    @endauth

</nav>
