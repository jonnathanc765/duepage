@extends('admin.layouts.app')

@section('nav.admin.contacts', 'active')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card shadow my-3">

                    <h3 class="my-3 text-center">Lista de personas que te han contactado</h3>

                    <hr>

                    <table class="table text-center p-2">
                        <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Nombre</th>
                                <th scope="col">Correo</th>
                                <th scope="col">Teléfono</th>
                                <th scope="col">Asunto</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($contacts as $contact)
                            <tr>
                                <th scope="row">
                                    {{ $loop->iteration }}
                                    @if ($contact->read == false)
                                    <span class="badge badge-danger">¡Nuevo!</span>
                                    @endif
                                </th>
                                <td>{{ $contact->name }}</td>
                                <td>{{ $contact->email }}</td>
                                <td>{{ $contact->phone }}</td>
                                <td>{{ $contact->subject }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>


                </div>
            </div>
        </div>
    </div>
@endsection
