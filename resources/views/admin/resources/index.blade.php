@extends('admin.layouts.app')

@section('nav.admin.resources', 'active')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card shadow my-3">

                    <h3 class="my-3 text-center">Lista de personas que ha descargado recursos</h3>

                    <hr>

                    <table class="table text-center p-2">
                        <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Nombre</th>
                                <th scope="col">Correo</th>
                                <th scope="col">Teléfono</th>
                                <th scope="col">Recurso Descargado</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($resources as $resource)
                            <tr>
                                <th scope="row">
                                    {{ $loop->iteration }}
                                    @if ($resource->read == false)
                                    <span class="badge badge-danger">¡Nuevo!</span>
                                    @endif
                                </th>
                                <td>{{ $resource->name }}</td>
                                <td>{{ $resource->email }}</td>
                                <td>{{ $resource->phone }}</td>
                                <td>{{ $resource->resource }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>


                </div>
            </div>
        </div>
    </div>
@endsection
