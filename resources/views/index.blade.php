@extends('layouts.app')

@section('head')
<!-- Customimized Style -->
    <link rel="stylesheet" href="{{ asset('css/styles.css') }}">
    <link rel="stylesheet" href="{{ asset('vendor/OwlCarousel/assets/owl.carousel.min.css') }}">
@endsection

@section('content')

    <div class="glow">
        <a href="#meet-me" class="smooth scroll-down">
            <i class="fas fa-chevron-down"></i>
        </a>

        <nav class="navbar navbar-expand-md">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                <i class="fas fa-bars"></i>
            </button>
            <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
                <div class="navbar-nav">
                    <a class="nav-item nav-link active" style="border-bottom:1px solid #fff" href="{{ route('duepage') }}#calculator">Calculadora Paypal <span class="sr-only">(current)</span></a>
                    <a class="nav-item nav-link" href="{{ route('duepage') }}#resources">Recursos descargables</a>
                </div>
            </div>
        </nav>
    </div>


    <header class="home">

        @include('layouts.header')

        <div class="left-circles">
            <img src="{{ asset('img/resources/two-circles.png') }}" alt="">
        </div>

        <div class="right-circle">
            <img src="{{ asset('img/resources/Recurso 55.png') }}" alt="">
        </div>

        <div class="left-circles">
            <img src="" alt="">
        </div>

        <div class="container welcome">
            <div class="row mt-5">
                <div class="col-md-6 d-flex justify-content-center" class="avatar">
                    <img src="{{ asset('img/Cesar/1.png') }}" alt="César Sánchez">
                </div>
                <div class="col-md-6 text">
                    <h2 class="im">Hola, Soy</h2>
                    <h2>
                        <strong>
                            César Sánchez
                        </strong>
                    </h2>
                    <h3>Economista</h3>
                    <a href="#meet-me" class="meet-me smooth">Conóceme</a>
                </div>
            </div>
        </div>



        <svg id="gris_medio" data-name="gris medio" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1025 198.29"><defs></defs><path class="cls-1" d="M1179,432.76q-6.78,6.5-14,12.2c-76.26,59.92-162,131-389.72,27C523.15,356.85,524,433.12,332,448.12c-44.9,3.51-145.34-13-177-49.41L156,597H1180Z" transform="translate(-155 -398.71)"/></svg>

        <div class="dots">
            <img src="{{ asset('img/resources/dots-light.png') }}" alt="">
        </div>

        <svg id="gris_claro" data-name="gris claro" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1024 128.34"><defs></defs><path class="cls-1" d="M1164.85,508.89c-76.26,59.92-207.14,76.89-389.72,1.51-256.2-105.78-251.25,11.66-443.25,26.66-44.91,3.5-145.35-13-177-49.42V595h1024V496.71C1174.33,501,1169.67,505.11,1164.85,508.89Z" transform="translate(-154.84 -466.66)"/></svg>

    </header>

    <main>

        {{-- Resumen  --}}
        <section class="section-1 resumen" id="meet-me">




            <div class="left-stain">
                <img src="{{ asset('img/resources/Recurso 46.png') }}" alt="">
            </div>

            <div class="right-stain">
                <img src="{{ asset('img/resources/Recurso 64.png') }}" alt="">
            </div>

            <div class="container">




                <div class="row">
                    <div class="col-md-6 d-flex flex-column justify-content-center px-5">
                        <h3 class="mb-1">Especialista en Bancas <br> y Finanzas</h3>
                        <p>
                            Soy economista de la Universidad Católica Andrés Bello. Empecé mi carrera laboral en la banca, en la cual trabajé 2 años como consultor estratégico en diversos bancos de Caracas.
                        </p>
                        <p>
                            Durante ese período, entendí que mi pasión apunta al mundo de las finanzas y todo lo que esto conlleva. La generación de estrategias financieras para mis clientes con el objetivo maximizar sus finanzas y optimizar su modelo de negocio.
                        </p>
                        <p>
                            El propósito de mi trabajo es compartir mis conocimientos y experiencias con mis clientes. Brindarles un excelente servicio e información de valor que pueda ser útil en el campo de las finanzas y banca.
                        </p>
                        <p>
                            La premisa de mi trabajo se basa en la responsabilidad, que las operaciones sean eficaces y sobre todo seguras.
                        </p>
                    </div>
                    <div class="col-md-6 d-flex justify-content-center align-items-center">
                        <img src="{{ asset('img/Cesar/2.jpg') }}" alt="">
                    </div>
                </div>

                <div class="row">
                    <div class="col-1 button-container d-flex justify-content-center align-content-center">
                        <button class="button gallery-carousel-prev">
                            <i class="fas fa-angle-double-left"></i>
                        </button>
                    </div>
                    <div class="col-10 gallery">
                        <div class="owl-carousel">
                            <img src="{{ asset('img/Cesar/carousel/1c.jpg') }}" alt="">
                            <img src="{{ asset('img/Cesar/carousel/2c.jpg') }}" alt="">
                            <img src="{{ asset('img/Cesar/carousel/3c.jpeg') }}" alt="">
                            <img src="{{ asset('img/Cesar/carousel/4c.jpeg') }}" alt="">
                            <img src="{{ asset('img/Cesar/carousel/5c.jpeg') }}" alt="">
                            <img src="{{ asset('img/Cesar/carousel/6c.jpeg') }}" alt="">
                        </div>
                    </div>
                    <div class="col-1 button-container d-flex justify-content-center align-content-center">
                        <button class="button gallery-carousel-next">
                            <i class="fas fa-angle-double-right"></i>
                        </button>
                    </div>
                </div>

            </div>



            <svg id="gris_claro" data-name="gris claro" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1024 128.34"><defs><style>.cls-1{fill:#ccc;}</style></defs><title>Sin título-1</title><path class="cls-1" d="M1164.85,508.89c-76.26,59.92-207.14,76.89-389.72,1.51-256.2-105.78-251.25,11.66-443.25,26.66-44.91,3.5-145.35-13-177-49.42V595h1024V496.71C1174.33,501,1169.67,505.11,1164.85,508.89Z" transform="translate(-154.84 -466.66)"/></svg>

        </section>
        {{-- Timeline  --}}
        <section class="timeline">

            <div class="left-stain">
                <img src="{{ asset('img/resources/Recurso 63.png') }}" alt="">
            </div>

            <div class="right-stain">
                <img src="{{ asset('img/resources/Recurso 66.png') }}" alt="">
            </div>

            <div class="left-circles">
                <img src="{{ asset('img/resources/Recurso 60.png') }}" alt="">
            </div>

            <div class="right-circle">
                <img src="{{ asset('img/resources/Recurso 61.png') }}" alt="">
            </div>


            <div class="container">
                <div class="row">
                    <div class="col-md-3">
                        <div class="line-1">
                            <div class="dot dot-up">
                            </div>
                            <div class="dot dot-down">
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-5 history history-1">
                        <img src="{{ asset('img/icons/born.png') }}" class="mb-3">
                        <h3>1992</h3>
                        <h4>Nacido en Lara, Barquisimeto</h4>
                        <p><i>Venezuela</i></p>
                        <p class="text-justify">
                            Nací en Barquisimeto, estado Lara. Desde pequeño me ha gustado el comercio; recuerdo que desde temprana edad, cuando mi mamá iba a la iglesia, yo me quedaba afuera junto a la señora que vendía las empanadas, mi trabajo era recibir el dinero o entregar los alimentos. Así comenzó todo
                        </p>
                    </div>
                    <div class="col-md-7 d-flex flex-column align-items-end" style="margin-top: 120px">
                        <div class="line-2">
                            <div class="dot dot-up"></div>
                            <div class="container">
                                <div class="row d-flex justify-content-end">
                                </div>
                            </div>
                        </div>
                        <div class="line-2-1">
                            <div class="dot">
                            </div>
                        </div>

                    </div>
                </div>
                <div class="row">
                    <div class="col-md-5 line-3-container">
                        <div class="line-3">
                            <div class="dot dot-down"></div>
                        </div>
                    </div>
                    <div class="col-md-7 history history-2-container">
                        <div class="col-md-8 col-12 history-2">
                            <img src="{{ asset('img/icons/stand.png') }}" class="mb-3">
                            <h3>2009</h3>
                            <h4>Comerciante innato</h4>
                            <p class="text-justify">
                                Fue en mi etapa colegial donde comencé a incursionar en el mundo de los negocios. Ese inicio me gusta llamarlo: el imperio de chucherías, ha sido una de las mejores experiencias que tuve, además esa práctica me enseñó el camino que ahora transito. Lo que había comenzado como una oportunidad se había convertido en un emporio de dulces , todo surgía tan bien que incluso le pedí a una compañera de clase que me ayudara con la distribución; mientras yo cobraba ella se encargaba de entregar la mercancía. Desafortunadamente, mi negocio fue clausurado por la coordinación del colegio alegando que la institución no era un mercado público

                            </p>
                        </div>
                    </div>
                </div>
                <div class="row mt-5">
                    <div class="col-md-5 history history-3">
                        <img src="{{ asset('img/icons/megaphone.png') }}" class="mb-3">
                        <h3>2014</h3>
                        <p class="text-justify">
                            La etapa universitaria fijó mi actitud y mis metas. Relacionarme con infinidad de personas, crear alianzas y desarrollar mis habilidades sociales fue algo que en definitiva logré en esta etapa. La dedicación y constancia me permitió ser coordinador de las Relaciones Públicas en el congreso de Economía UCAB organizado para 500 personas.
                        </p>
                    </div>
                </div>

                <div class="row d-flex justify-content-end">
                    <div class="col-md-5 d-flex align-items-end flex-column">
                        <div class="line-4">
                            <div class="dot"></div>
                        </div>
                        <div class="line-5">
                            <div class="history-4 history">
                                <img src="{{ asset('img/icons/graduation.png') }}" class="mb-3">
                                <h3>2016</h3>
                                <h4>Economista UCAB</h4>
                            </div>
                            <div class="dot"></div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 d-flex justify-content-end">
                        <div class="line-6">
                            <div class="dot"></div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-5 mb-5 history history-5">
                        <img src="{{ asset('img/icons/bank.png') }}" class="mb-3">
                        <p class="text-justify">
                            Culminar un objetivo, la sensación del deber cumplido y la constante pregunta: ¿y ahora qué? Afortunadamente había hecho pasantías en el departamento de desarrollo de nuevos negocios en el BOD, lo que me permitió entrar en el campo estratégico y financiero de la banca. A partir de entonces ingresé en el mundo de la consultoría estratégica en la banca.
                        </p>
                    </div>
                </div>
                <div class="row justify-content-around">
                    <div class="col-md-4 line-7-container">
                        <div class="line-7">
                            <div class="dot dot-up"></div>
                            <div class="dot dot-down"></div>
                        </div>
                    </div>
                    <div class="col-md-5 history history-6">
                        <img src="{{ asset('img/icons/money-up.png') }}" class="mb-3">
                        <h3>2017 - Actualidad</h3>
                        <h4>Especialización en Banca Venezolana</h4>
                        <p><i>Banca Estadounidense </i></p>
                        <p class="text-justify">
                            Entendiendo el entorno macroeconómico que vive el país y constantemente analizando el flujo financiero personal y empresarial de los venezolanos entre la banca venezolana y norteamericana, me he dedicado a asesorar en aspectos financieros de cobertura de capital y mecanismos transaccionales para mejorar el estilo de vida y el negocio de mis clientes.
                        </p>
                    </div>
                </div>
            </div>
        </section>
        {{-- Timeline responsive --}}
        <section class="timeline-responsive" style="display: none">

            <div class="left-stain">
                <img src="{{ asset('img/resources/Recurso 63.png') }}" alt="">
            </div>

            <div class="right-stain">
                <img src="{{ asset('img/resources/Recurso 66.png') }}" alt="">
            </div>

            <div class="left-circles">
                <img src="{{ asset('img/resources/Recurso 60.png') }}" alt="">
            </div>

            <div class="right-circle">
                <img src="{{ asset('img/resources/Recurso 61.png') }}" alt="">
            </div>


            <div class="container">
                <div class="line-container">
                    <div class="row">

                        <div class="col-12 item extreme-up">
                            <div class="dot"></div>
                        </div>

                        <br><br><br><br><br>

                        <div class="col-12 item">
                            <div class="dot"></div>
                            <img src="{{ asset('img/icons/born.png') }}" class="mb-3">
                            <h3>1992</h3>
                            <h4>Nacido en Lara, Barquisimeto</h4>
                            <p><i>Venezuela</i></p>
                            <p class="text-justify">
                                Nací en Barquisimeto, estado Lara. Desde pequeño me ha gustado el comercio; recuerdo que desde temprana edad, cuando mi mamá iba a la iglesia, yo me quedaba afuera junto a la señora que vendía las empanadas, mi trabajo era recibir el dinero o entregar los alimentos. Así comenzó todo
                            </p>
                        </div>

                        <div class="col-12 item">
                            <div class="dot"></div>
                            <img src="{{ asset('img/icons/stand.png') }}" class="mb-3">
                            <h3>2009</h3>
                            <h4>Comerciante innato</h4>
                            <p class="text-justify">
                                Fue en mi etapa colegial donde comencé a incursionar en el mundo de los negocios. Ese inicio me gusta llamarlo: el imperio de chucherías, ha sido una de las mejores experiencias que tuve, además esa práctica me enseñó el camino que ahora transito. Lo que había comenzado como una oportunidad se había convertido en un emporio de dulces , todo surgía tan bien que incluso le pedí a una compañera de clase que me ayudara con la distribución; mientras yo cobraba ella se encargaba de entregar la mercancía. Desafortunadamente, mi negocio fue clausurado por la coordinación del colegio alegando que la institución no era un mercado público
                            </p>
                        </div>

                        <div class="col-12 item">
                            <div class="dot"></div>
                            <img src="{{ asset('img/icons/megaphone.png') }}" class="mb-3">
                            <h3>2014</h3>
                            <p class="text-justify">
                                La etapa universitaria fijó mi actitud y mis metas. Relacionarme con infinidad de personas, crear alianzas y desarrollar mis habilidades sociales fue algo que en definitiva logré en esta etapa. La dedicación y constancia me permitió ser coordinador de las Relaciones Públicas en el congreso de Economía UCAB organizado para 500 personas.
                            </p>
                        </div>

                        <div class="col-12 item">
                            <div class="dot"></div>
                            <img src="{{ asset('img/icons/megaphone.png') }}" class="mb-3">
                            <h3>2016</h3>
                            <p>Economista UCAB</p>
                        </div>

                        <div class="col-12 item">
                            <div class="dot"></div>
                            <img src="{{ asset('img/icons/bank.png') }}" class="mb-3">
                            <p class="text-justify">
                                Culminar un objetivo, la sensación del deber cumplido y la sempiterna pregunta: ¿y ahora qué? Afortunadamente había hecho pasantías en el departamento de desarrollo de nuevos negocios en el BOD, lo que me permitió entrar en el campo estratégico y financiero de la banca. A partir de entonces ingresé en el mundo de la consultoría estratégica en la banca.
                            </p>
                        </div>


                        <div class="col-12 item">
                            <div class="dot"></div>
                            <img src="{{ asset('img/icons/money-up.png') }}" class="mb-3">
                            <h3>2017 - Actualidad</h3>
                            <h4>Especialización en Banca Venezolana - Banca Estadounidense </h4>
                            <p class="text-justify">
                                Entendiendo el entorno macroeconómico que vive el país y constantemente analizando el flujo financiero personal y empresarial de los venezolanos entre la banca venezolana y norteamericana, me he dedicado a asesorar en aspectos financieros de cobertura de capital y mecanismos transaccionales para mejorar el estilo de vida y el negocio de mis clientes.
                            </p>
                        </div>



                        <div class="col-12 item extreme-down">
                            <div class="dot"></div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        {{-- philosophy --}}
        <section class="philosophy">
            <div class="dots">
                <img src="{{ asset('img/resources/dots-super-light.png') }}" alt="">
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h2 class="text-center">Mi Filosofía</h2>
                    </div>

                    <div class="col-md-12 text-center">
                        <img src="{{ asset('img/icons/quote.png') }}" alt="">
                    </div>

                    <div class="row d-flex justify-content-center">

                        <div class="col-md-8 col-12">
                            <p>
                                Mi principio de vida es ser alguien ético y poder ayudar a mi comunidad siendo el mejor en lo que hago. Orientado al servicio de los clientes e intentando entender y buscar una solución a sus necesidades.
                            </p>
                            <p>
                                Poner mi nombre en alto, con mucha responsabilidad, asumir el compromiso de cumplir con los objetivos  propuestos, sentir que aporto valor agregado a las personas, que lo valoren y construir esa relación de confianza con el cliente es, definitivamente el propósito de mi trabajo.
                            </p>
                            <h3 class="text-center">César Sánchez</h3>
                            <h5 class="text-center">Especialista en Bancas y Finanzas</h5>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        {{-- Testimoniales  --}}
        <section class="testimonials" id="testimonials">

            <div class="container">
                <div class="row">
                    <div class="col-md-12 my-4">
                        <h2 class="text-center">Testimonios</h2>
                        <div class="row">
                            <div class="col-md-12 d-flex justify-content-center mt-3">
                                <div class="bar"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-1 d-flex justify-content-center align-items-center">
                        <button class="carousel-prev"><i class="fas fa-angle-double-left"></i></button>
                    </div>
                    <div class="col-10 testimonials-carousel d-flex justify-content-center align-items-center">

                        <div class="owl-carousel">

                            <div class="item">
                                <iframe src="https://www.youtube.com/embed/LMhj44B0dMc">
                                </iframe>
                                <h4>Kingberly Pérez</h4>
                                <p>
                                    {{-- No conozco a nadie que sepa más que César sobre el manejo operativo y transferencia de dinero en la banca norteamericana y venezolana. Su trabajo es impecable, es muy rápido y de confianza. --}}
                                </p>
                            </div>
                            <div class="item">
                                <iframe src="https://www.youtube.com/embed/QVnaoH3Cyeg">
                                </iframe>
                                <h4>Carlos Navarro</h4>
                                <p>
                                    {{-- Venezolano, economista, especialista en banca y finanzas. Consultor estratégico en la banca venezolana y estadounidense. --}}
                                </p>
                            </div>

                        </div>

                    </div>
                    <div class="col-1 d-flex justify-content-center align-items-center">
                        <button class="carousel-next"><i class="fas fa-angle-double-right"></i></button>
                    </div>

                </div>
            </div>
        </section>
        {{-- Preguntas frecuentes  --}}
        <section class="faqs" id="faqs">
            <div class="dot">
                <img src="{{ asset('img/resources/Recurso 66.png') }}" alt="">
            </div>

            <div class="container">
                <div class="row">
                    <div class="col-md-7 faqs-list">
                        <h3>Preguntas Frecuentes</h3>

                        <ul>
                            <li>
                                <a
                                 data-toggle="collapse"
                                  href="#collapeseOne" role="button" aria-expanded="true" aria-controls="collapeseOne">
                                    <i class="fas fa-plus"></i> ¿Puedo abrir una cuenta en dólares desde Venezuela?
                                </a>
                                <div class="collapse show" id="collapeseOne">
                                    <p>
                                        Sí, una de las opciones  es Mercantil Panamá donde ofrece la opción de abrir una cuenta en USD si tienes más de un año con una cuenta en mercantil Venezuela. Para abrir la cuenta te pedirán 2500 USD y posteriormente mantener $1000 USD para que no te cobren el mínimo de mantenimiento. Todo el proceso se realiza online por: <a href="https://www.mercantilbanco.com.pa/">Mercantil Banco</a>
                                    </p>
                                </div>
                            </li>
                            <li>
                                <a
                                 data-toggle="collapse"
                                  href="#collapseTwo" role="button" aria-expanded="false" aria-controls="collapseTwo">
                                    <i class="fas fa-plus"></i> ¿Cuáles son los principales bancos en el extranjero utilizados por los venezolanos?
                                </a>
                                <div class="collapse" id="collapseTwo">
                                    <p>
                                        De Estados Unidos: Bank of America y Chase Bank. Los requisitos pueden variar según el estado donde esté la oficina, pero de los más flexibles como Florida, te solicitarán tu pasaporte y una factura con una dirección de Estados Unidos. Adicionalmente, hay un importante número de venezolanos que se abren cuenta en la plataforma Paypal para poder guardar y realizar transacciones en moneda extranjera, cuenta que se puede abrir online.
                                    </p>
                                </div>
                            </li>
                            <li>
                                <a
                                 data-toggle="collapse"
                                  href="#collapseThree" role="button" aria-expanded="false" aria-controls="collapseThree">
                                    <i class="fas fa-plus"></i> ¿Por qué la cotización del $ PayPal es menor a la de otros?
                                </a>
                                <div class="collapse" id="collapseThree">
                                    <p>Oferta y demanda</p>
                                    <ul>
                                        <li>El mercado de PayPal es más reducido, por lo tanto, su demanda es inferior.</li>
                                        <li>
                                            Hay pocos compradores, dado que debes tener una cuenta paypal verificada en los Estados Unidos para que puedas mover el dinero de esta cuenta PayPal a una cuenta en los Estados Unidos, por ejemplo Bofa
                                        </li>
                                        <li>
                                            Los compradores de $ PayPal no están generalizados, sino concentrados en gente que se dedica a comprar $ en esta modalidad, por lo tanto, establecen una prima adicional al precio.
                                        </li>
                                    </ul>
                                </div>
                            </li>
                            <li>
                                <a
                                 data-toggle="collapse"
                                  href="#collapseFour" role="button" aria-expanded="false" aria-controls="collapseFour">
                                    <i class="fas fa-plus"></i> ¿Cuál es el tamaño de Mercado de remesas venezolanas?
                                </a>
                                <div class="collapse" id="collapseFour">
                                    <p>
                                        El mercado de remesas hacia Venezuela ha aumentado en más de %4500 comparado con el 2016. La firma de estudios económicos Ecoanalitica estima:
                                    </p>

                                    <ul>
                                        <li>2016: $78 millones</li>
                                        <li>2017: $1.138 millones</li>
                                        <li>2018: $2.500 millones</li>
                                        <li>2019: $3.700 millones</li>
                                    </ul>
                                    <p>
                                        A la fecha, son aproximadamente 4 millones de inmigrantes venezolanos, teniendo mayor presencia en Colombia, Perú y Estados Unidos. Cabe acotar, que, sacando un promedio por emigrante, daría $925 por año y $77 enviado mensualmente.
                                    </p>
                                </div>
                            </li>
                            <li>
                                <a
                                 data-toggle="collapse"
                                  href="#collapseFive" role="button" aria-expanded="false" aria-controls="collapseFive">
                                    <i class="fas fa-plus"></i> ¿Cuáles son los principales actores que afectan la cotización del dólar en Venezuela?
                                </a>
                                <div class="collapse" id="collapseFive">
                                    <p>
                                            Dado las condiciones macroeconómicas de Venezuela, la cotización del dólar es muy variante, aun así hay 3 principales actores:
                                    </p>
                                    <ul>
                                        <li>El Presidente de la República: cambiando el marco regulatorio cambiario o afectar las expectativas puede afectar directamente la cotización.</li>
                                        <li>El Banco Central de Venezuela: La realización de política monetaria, a través de inyección de liquidez al mercado (+ Bolívares) o la contracción de la misma (+ dólares o moneda dura) al mercado.</li>
                                        <li>Mesas de Cambio y Casas de Cambio de Cúcuta; Los principales agentes privados son estos, los cuáles compran o venden mayormente por la disponibilidad de bolívares o las expectativas de la tendencia del dólar, los demás agentes son precios aceptantes a las cotizaciones que marcan estas casas de cambio y a las dinámicas de mercado.</li>
                                    </ul>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <div class="col-md-5 faqs-image">
                        <img src="{{ asset('img/Cesar/3.png') }}" alt="">
                    </div>
                </div>
            </div>


            <svg id="Capa_1" data-name="Capa 1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 1000 144.75">
                <defs>
                    <style>
                        .cls-1{fill:url(#gradient);}.cls-2{clip-path:url(#clip-path);}.cls-3{fill:url(#gradient-3);}
                    </style>
                    <linearGradient id="gradient" y1="98.58" x2="1000" y2="98.58" gradientUnits="userSpaceOnUse">
                        <stop offset="0" stop-color="#4d4d4d"/>
                        <stop offset="1" stop-color="#999"/>
                    </linearGradient>
                    <clipPath id="clip-path" transform="translate(0 -26.21)">
                        <path class="cls-1" d="M581.41,82.74C310.48,224.34,0,112,0,112V171H1000V110.2s-73.35-84-211.53-84c-57.86,0-127.09,14.73-207.06,56.53"/>
                    </clipPath>
                </defs>
                <path class="cls-1" d="M581.41,82.74C310.48,224.34,0,112,0,112V171H1000V110.2s-73.35-84-211.53-84c-57.86,0-127.09,14.73-207.06,56.53" transform="translate(0 -26.21)"/>
                <g class="cls-2">
                    <rect class="cls-3" width="1000" height="144.75"/>
                </g>
            </svg>

        </section>
        {{-- Formulario de contacto  --}}
        <section class="contact-form" id="contact-me">

            <div class="container">

                <div class="row my-3">
                    <div class="col-md-12 text-center">
                        <h3>¡Charlemos!</h3>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">


                        <form autocomplete="off" action="{{ route('contacts.store') }}" method="POST">
                            @csrf
                            <div class="form-group">
                                <div class="row">
                                    <div class="col">
                                        <input value="{{ old('name') }}" type="text" class="form-control @error('name') is-invalid @enderror" name="name" placeholder="Nombre">
                                        @error('name')
                                        <div class="invalid-feedback">
                                            {{ $message }}
                                        </div>
                                        @enderror
                                    </div>
                                    <div class="col">
                                        <input value="{{ old('phone') }}" type="text" class="form-control @error('phone') is-invalid @enderror" name="phone" placeholder="Nùmero de telèfono">
                                        @error('phone')
                                        <div class="invalid-feedback">
                                            {{ $message }}
                                        </div>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <input value="{{ old('email') }}" type="email" class="form-control @error('email') is-invalid @enderror" name="email" placeholder="Correo">
                                @error('email')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div>
                                @enderror
                            </div>

                            <div class="form-group">
                                <input value="{{ old('subject') }}" type="text" class="form-control @error('subject') is-invalid @enderror" name="subject" placeholder="Asunto">
                                @error('subject')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div>
                                @enderror
                            </div>

                            <div class="form-group">
                                <textarea placeholder="Mensaje" name="message" class="form-control @error('message') is-invalid @enderror">{{ old('message') }}</textarea>
                                @error('message')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div>
                                @enderror
                            </div>

                            <div class="form-group checkbox-group">
                                <input disabled checked type="checkbox" id="term">
                                <label for="term">Al marcar esto, acepta nuestros términos y política de privacidad </label>
                            </div>

                            <button class="btn btn-submit" type="submit">Enviar mensaje</button>

                        </form>

                    </div>
                    <div class="col-md-6 d-flex justify-content-center align-items-center mt-3">
                        <img src="{{ asset('img/smartphone.png') }}" alt="">
                    </div>
                </div>



            </div>

        </section>

    </main>

    @include('layouts.footer')

@endsection
