@if ($errors->any())
<script src="{{ asset('js/sweetalert.js') }}"></script>
<script>
    Swal.fire('Tenemos un problema...', 'Debe llenar todos los campos correctamente',
        'error');
</script>
@endif

@if (Session::has('success'))
<script src="{{ asset('js/sweetalert.js') }}"></script>
<script>
        Swal.fire("¡Todo salió bien!", "{{ Session::get('success') }}", "success");
</script>
@endif
