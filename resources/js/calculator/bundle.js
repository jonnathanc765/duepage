window.Vue = require('vue');

Vue.component('calculator-form', require('./calculator-form.vue').default);

const app = new Vue({
    el: '#app',
});
