
$(document).ready(function () {

    $('form button').click(function() {
        $(this).parents('form').submit();
        $(this).prop("disabled", true);
    });

    applyStickyNavigation();

    var owl = $(".testimonials .owl-carousel");

    owl.owlCarousel({
        loop: true,
        responsive:{
        330: {
            items: 1
        },
        370: {
            items: 1
        },
        600:{
            items: 1
        },
        1000:{
            items: 2
        }
    }
    });

    $('.carousel-prev').click(function() {
        owl.trigger('prev.owl.carousel');
    });
    // Go to the previous item
    $('.carousel-next').click(function() {
        owl.trigger('next.owl.carousel');
    });

    $('.faqs li a').click(function() {
        $(this).parents('ul').find('li .collapse').collapse('hide');

    });

});


$(document).ready(function(){
    var owl = $(".gallery .owl-carousel");
    owl.owlCarousel({
        loop: true,
        margin: 30,
        dots: false,
        responsive: {
            991: {
                items: 3
            },
            600: {
                items: 2

            },
            0: {
                items: 1,
                margin: 0
            }
        },
        lazyLoad: true,
        autoplay: true
    });

    $('.gallery-carousel-prev').click(function() {
        owl.trigger('prev.owl.carousel');
    });
    // Go to the previous item
    $('.gallery-carousel-next').click(function() {
        owl.trigger('next.owl.carousel');
    });
  })


var lnStickyNavigation;

function applyStickyNavigation()
{
    lnStickyNavigation = $('.scroll-down').offset().top + 20;

    $(window).on('scroll', function()
    {
        stickyNavigation();
    });

    stickyNavigation();
}

function stickyNavigation()
{
    if($(window).scrollTop() > lnStickyNavigation)
    {
        $('.glow').addClass('fixed');
    }
    else
    {
        $('.glow').removeClass('fixed');
    }
}


