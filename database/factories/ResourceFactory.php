<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Resource;
use Faker\Generator as Faker;


$factory->define(Resource::class, function (Faker $faker) {

    $resources = ['Limites de Bancos de venezuela', 'Limites de Zelle', 'Comisiones de transferencias de Paypal'];

    return [
        'name' => $faker->name,
        'email' => $faker->safeEmail,
        'phone' => $faker->phoneNumber,
        'resource' => $faker->randomElement($resources),
        'read' => rand(0,1)
    ];
});
