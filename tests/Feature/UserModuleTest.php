<?php

namespace Tests\Feature;

use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class UserModuleTest extends TestCase
{
    use RefreshDatabase;
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_it_loads_the_login_form()
    {
        $this->get(route('login'))
        ->assertStatus(200)
        ->assertSee('Ingresar al sistema')
        ->assertSee('Correo')
        ->assertSee('Contraseña')
        ->assertSee('Entrar');
    }
    public function test_it_login_a_user()
    {
        $user = factory(User::class)->create([
            'email' => 'john@gmail.com'
        ]);

       $this->post(route('login', [
           'email' => 'john@gmail.com',
           'password' => 'password'
       ]))
       ->assertRedirect(route('admin.index'));

       $this->assertAuthenticatedAs($user);
    }
}
