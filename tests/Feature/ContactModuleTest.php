<?php

namespace Tests\Feature;

use App\Contact;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ContactModuleTest extends TestCase
{
    use RefreshDatabase;
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_it_show_a_list_of_contacts()
    {
        $this->withoutExceptionHandling();
        $this->actingAs($this->createUser());

        factory(Contact::class)->create([
            'name' => 'Joel',
            'subject' => 'Este es el asunto'
        ]);
        factory(Contact::class, 40)->create();

        $this->get(route('admin.contacts.index'))
        ->assertStatus(200)
        ->assertSee('Lista de personas que te han contactado')
        ->assertSee('Joel')
        ->assertSee('Este es el asunto');
    }
    public function test_it_create_a_contact()
    {
        $this->withoutExceptionHandling();
        $data = [
            'name' => 'Josh Doe',
            'email' => "josh@gmail.com",
            'phone' => "434343132",
            'subject' => 'This is the subject',
            'message' => 'This is de message',
        ];

        $this->post(route('contacts.store', $data))
        ->assertRedirect(route('home'))
        ->assertSessionHas('success', 'Su mensaje se ha enviado exitosamente, nos pondremos en contacto contigo los mas pronto posible');

        $this->assertDatabaseHas('contacts', $data);

    }
    public function createUser()
    {
        $user = factory(User::class)->create();
        return $user;
    }
}
