<?php

namespace Tests\Feature;

use App\Resource;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ResourceModuleTest extends TestCase
{
    use RefreshDatabase;
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_it_listing_all_resources()
    {
        $this->actingAs($this->createUser());

        factory(Resource::class)->create([
            'name' => 'Joel',
            'resource' => 'Limites de zelle',
        ]);

        factory(Resource::class)->create([
            'name' => 'Karen',
            'resource' => 'Limites de bancos de Venezuela',
        ]);

        $this->get(route('admin.resources.index'))
        ->assertStatus(200)
        ->assertSee('Lista de personas que ha descargado recursos')
        ->assertSee('Limites de zelle')
        ->assertSee('Joel')
        ->assertSee('Limites de bancos de Venezuela')
        ->assertSee('Karen');

    }
    public function test_it_create_a_resource()
    {
        $data = [
            'name' => 'John Doe',
            'email' => 'john@gmail.com',
            'phone' => '04126567643',
            'resource' => "Limites de bancos venezolanos",
        ];

        $this->post(route('resources.store', $data))
        ->assertRedirect(route('duepage'))
        ->assertSessionHas('success', 'Tu recurso se ha enviado a tu correo, revisa tu bandeja de entrada o spam');

        $this->assertDatabaseHas('resources' , $data);
    }

    public function createUser()
    {
        $user = factory(User::class)->create();
        return $user;
    }
}
